﻿using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Drawing;

namespace BippInterface
{


    class BIPP
    {
        Connection C = new Connection();


        // Load's do Produto principal

        public Image LoadImage(Int64 ID)
        {
            Image image;
            try
            {

                MySqlDataReader reader;
                C.Connect.Open();
                C.cmd = new MySqlCommand("SELECT foto " +
                                         "FROM produto " +
                                         "WHERE id = " + ID, C.Connect);

                reader = C.cmd.ExecuteReader();
                reader.Read();
                image = (Bitmap)new ImageConverter().ConvertFrom(reader.GetValue(0));

                return image;
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.Message);
                return image = null;
            }
            finally
            {
                C.Connect.Close();
                C.Connect.Dispose();
            }
        }

        public Int32 LoadID(String EAN)
        {
            Int32 i;
            try
            {
                MySqlDataReader reader;
                C.cmd = new MySqlCommand("SELECT id " +
                                         "FROM produto " +
                                         "WHERE EAN LIKE " + EAN, C.Connect);

                C.Connect.Open();
                reader = C.cmd.ExecuteReader();
                reader.Read();
                i = (Int32)reader.GetValue(0);

                return i;
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.Message);
                return 0;
            }
            finally
            {
                C.Connect.Dispose();
                C.Connect.Close();
            }
        }

        public Int64 LoadIDProd(Int64 ID)
        {
            Int64 i;
            try
            {
                MySqlDataReader reader;
                C.cmd = new MySqlCommand("SELECT id " +
                                         "FROM produto " +
                                         "WHERE id LIKE " + ID, C.Connect);

                C.Connect.Open();
                reader = C.cmd.ExecuteReader();
                reader.Read();
                i = (Int32)reader.GetValue(0);

                return i;
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.Message);
                return 0;
            }
            finally
            {
                C.Connect.Dispose();
                C.Connect.Close();
            }
        }

        public string LoadDescription(Int64 ID)
        {
            string S;
            try
            {
                MySqlDataReader reader;
                C.cmd = new MySqlCommand("SELECT descricao " +
                                         "FROM produto " +
                                         "WHERE id = " + ID, C.Connect);

                C.Connect.Open();
                reader = C.cmd.ExecuteReader();
                reader.Read();
                S = (string)reader.GetValue(0);

                return S;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
            finally
            {
                C.Connect.Dispose();
                C.Connect.Close();
            }
        }

        public Double LoadPrice(Int64 ID, int id_loja)
        {
            Double S;
            try
            {
                MySqlDataReader reader;
                C.cmd = new MySqlCommand("SELECT preco " +
                                         "FROM preco_loja " +
                                         "WHERE id_prod = " + ID + " AND id_loja = " + id_loja, C.Connect);

                C.Connect.Open();
                reader = C.cmd.ExecuteReader();
                reader.Read();
                S = (Double)reader.GetValue(0);

                return S;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
            finally
            {
                C.Connect.Dispose();
                C.Connect.Close();
            }
        } 

        // Load's do Produto Equivalente

        public IDataReader LoadIDEquiv(Int64 id_equival, int id_loja)
        {
            try

            {
                MySqlDataReader reader;
                C.cmd = new MySqlCommand("SELECT id_equival " +
                                        "FROM bipp " +
                                        "INNER JOIN produto ON produto.id = id_equival " +
                                        "JOIN preco_loja ON produto.id = preco_loja.id_prod " +
                                        "WHERE preco_loja.id_loja = " + id_loja + " AND bipp.id_prod = " + id_equival + " " +
                                        "ORDER BY RAND() limit 3", C.Connect);

                //SELECT id_prod, id_equival, id_loja, descricao, equiv FROM bipp
                //INNER JOIN produto ON produto.id = id_equival
                //WHERE id_loja = 1 AND id_prod = 58

                C.Connect.Open();
                reader = C.cmd.ExecuteReader();
                var DT = new DataTable();
                DT.Load(reader);

                return DT.CreateDataReader();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
            finally
            {
                C.Connect.Dispose();
                C.Connect.Close();
            }
        }

        public string LoadDescriptionEquiv(Int64 ID)
        {
            string S;
            try

            {
                MySqlDataReader reader;
                C.cmd = new MySqlCommand("SELECT descricao " +
                                         "FROM produto " +
                                         "WHERE id = " + ID, C.Connect);

                C.Connect.Open();
                reader = C.cmd.ExecuteReader();
                reader.Read();
                S = (string)reader.GetValue(0);

                return S;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);



                return null;
            }
            finally
            {
                C.Connect.Dispose();
                C.Connect.Close();
            }
        }

        public Image LoadImageEquiv(Int64 ID)
        {
            Image image;
            try
            {

                MySqlDataReader reader;
                C.Connect.Open();
                C.cmd = new MySqlCommand("SELECT foto, descricao " +
                                         "FROM produto " +
                                         "WHERE id = " + ID, C.Connect);

                reader = C.cmd.ExecuteReader();
                reader.Read();
                image = (Bitmap)new ImageConverter().ConvertFrom(reader.GetValue(0));

                return image;
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.Message);
                return image = null;
            }
            finally
            {
                C.Connect.Close();
                C.Connect.Dispose();
            }
        }


        public Double LoadPriceEquiv(Int64 ID, int id_loja)
        {
            Double S;
            try
            {
                MySqlDataReader reader;
                C.cmd = new MySqlCommand("SELECT preco FROM produto " +
                    "JOIN preco_loja ON produto.id = preco_loja.id_prod " + 
                    "WHERE produto.id = " + ID + " AND preco_loja.id_loja = " + id_loja, C.Connect);

                C.Connect.Open();
                reader = C.cmd.ExecuteReader();
                reader.Read();
                S = (Double)reader.GetValue(0);

                return S;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
            finally
            {
                C.Connect.Dispose();
                C.Connect.Close();
            }
        }
    }
}

