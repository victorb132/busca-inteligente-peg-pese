﻿namespace BippInterface
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.PbImg = new System.Windows.Forms.PictureBox();
            this.LblPrice = new System.Windows.Forms.Label();
            this.LblDesc = new System.Windows.Forms.Label();
            this.cmbLoja = new System.Windows.Forms.ComboBox();
            this.LblPrice3 = new System.Windows.Forms.Label();
            this.PbImg3 = new System.Windows.Forms.PictureBox();
            this.LblDesc3 = new System.Windows.Forms.Label();
            this.LblPrice2 = new System.Windows.Forms.Label();
            this.PbImg2 = new System.Windows.Forms.PictureBox();
            this.LblDesc2 = new System.Windows.Forms.Label();
            this.txtScan = new System.Windows.Forms.TextBox();
            this.LblDesc1 = new System.Windows.Forms.Label();
            this.PbImg1 = new System.Windows.Forms.PictureBox();
            this.LblPrice1 = new System.Windows.Forms.Label();
            this.Panel = new System.Windows.Forms.Panel();
            this.LblR = new System.Windows.Forms.Label();
            this.Panel3 = new System.Windows.Forms.Panel();
            this.Panel2 = new System.Windows.Forms.Panel();
            this.Panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.PbImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PbImg3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PbImg2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PbImg1)).BeginInit();
            this.Panel.SuspendLayout();
            this.Panel3.SuspendLayout();
            this.Panel2.SuspendLayout();
            this.Panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // PbImg
            // 
            this.PbImg.BackColor = System.Drawing.Color.Lime;
            this.PbImg.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PbImg.BackgroundImage")));
            this.PbImg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PbImg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PbImg.Location = new System.Drawing.Point(3, 3);
            this.PbImg.Name = "PbImg";
            this.PbImg.Size = new System.Drawing.Size(375, 375);
            this.PbImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PbImg.TabIndex = 0;
            this.PbImg.TabStop = false;
            // 
            // LblPrice
            // 
            this.LblPrice.AutoSize = true;
            this.LblPrice.Font = new System.Drawing.Font("VAGRounded BT", 69.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPrice.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.LblPrice.Location = new System.Drawing.Point(85, 511);
            this.LblPrice.Margin = new System.Windows.Forms.Padding(0);
            this.LblPrice.Name = "LblPrice";
            this.LblPrice.Size = new System.Drawing.Size(289, 114);
            this.LblPrice.TabIndex = 3;
            this.LblPrice.Text = "Preço";
            // 
            // LblDesc
            // 
            this.LblDesc.AutoSize = true;
            this.LblDesc.Font = new System.Drawing.Font("Verdana", 50.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDesc.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.LblDesc.Location = new System.Drawing.Point(9, 450);
            this.LblDesc.Name = "LblDesc";
            this.LblDesc.Size = new System.Drawing.Size(359, 80);
            this.LblDesc.TabIndex = 1;
            this.LblDesc.Text = "Descrição";
            // 
            // cmbLoja
            // 
            this.cmbLoja.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.cmbLoja.DropDownHeight = 200;
            this.cmbLoja.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLoja.DropDownWidth = 21;
            this.cmbLoja.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbLoja.ForeColor = System.Drawing.SystemColors.InfoText;
            this.cmbLoja.FormattingEnabled = true;
            this.cmbLoja.IntegralHeight = false;
            this.cmbLoja.Location = new System.Drawing.Point(0, 0);
            this.cmbLoja.Name = "cmbLoja";
            this.cmbLoja.Size = new System.Drawing.Size(63, 50);
            this.cmbLoja.TabIndex = 6;
            this.cmbLoja.SelectedIndexChanged += new System.EventHandler(this.cmbLoja_SelectedIndexChanged);
            // 
            // LblPrice3
            // 
            this.LblPrice3.AutoSize = true;
            this.LblPrice3.Font = new System.Drawing.Font("VAGRounded BT", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPrice3.Location = new System.Drawing.Point(192, 84);
            this.LblPrice3.Name = "LblPrice3";
            this.LblPrice3.Size = new System.Drawing.Size(249, 98);
            this.LblPrice3.TabIndex = 15;
            this.LblPrice3.Text = "Preço";
            this.LblPrice3.Click += new System.EventHandler(this.LblPrice3_Click);
            // 
            // PbImg3
            // 
            this.PbImg3.BackColor = System.Drawing.Color.Lime;
            this.PbImg3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PbImg3.BackgroundImage")));
            this.PbImg3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PbImg3.Location = new System.Drawing.Point(0, -2);
            this.PbImg3.Name = "PbImg3";
            this.PbImg3.Size = new System.Drawing.Size(180, 180);
            this.PbImg3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PbImg3.TabIndex = 0;
            this.PbImg3.TabStop = false;
            this.PbImg3.Click += new System.EventHandler(this.PbImg3_Click);
            // 
            // LblDesc3
            // 
            this.LblDesc3.AutoSize = true;
            this.LblDesc3.Font = new System.Drawing.Font("VAGRounded BT", 39.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDesc3.Location = new System.Drawing.Point(192, 25);
            this.LblDesc3.Name = "LblDesc3";
            this.LblDesc3.Size = new System.Drawing.Size(266, 64);
            this.LblDesc3.TabIndex = 13;
            this.LblDesc3.Text = "Descrição";
            this.LblDesc3.Click += new System.EventHandler(this.LblDesc3_Click);
            // 
            // LblPrice2
            // 
            this.LblPrice2.AutoSize = true;
            this.LblPrice2.Font = new System.Drawing.Font("VAGRounded BT", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPrice2.Location = new System.Drawing.Point(192, 95);
            this.LblPrice2.Name = "LblPrice2";
            this.LblPrice2.Size = new System.Drawing.Size(249, 98);
            this.LblPrice2.TabIndex = 11;
            this.LblPrice2.Text = "Preço";
            this.LblPrice2.Click += new System.EventHandler(this.LblPrice2_Click);
            // 
            // PbImg2
            // 
            this.PbImg2.BackColor = System.Drawing.Color.Lime;
            this.PbImg2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PbImg2.BackgroundImage")));
            this.PbImg2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PbImg2.Location = new System.Drawing.Point(0, 0);
            this.PbImg2.Name = "PbImg2";
            this.PbImg2.Size = new System.Drawing.Size(180, 180);
            this.PbImg2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PbImg2.TabIndex = 0;
            this.PbImg2.TabStop = false;
            this.PbImg2.Click += new System.EventHandler(this.PbImg2_Click);
            // 
            // LblDesc2
            // 
            this.LblDesc2.AutoSize = true;
            this.LblDesc2.Font = new System.Drawing.Font("VAGRounded BT", 39.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDesc2.Location = new System.Drawing.Point(408, 247);
            this.LblDesc2.Name = "LblDesc2";
            this.LblDesc2.Size = new System.Drawing.Size(266, 64);
            this.LblDesc2.TabIndex = 9;
            this.LblDesc2.Text = "Descrição";
            this.LblDesc2.Click += new System.EventHandler(this.LblDesc2_Click);
            // 
            // txtScan
            // 
            this.txtScan.Location = new System.Drawing.Point(-20, -20);
            this.txtScan.Name = "txtScan";
            this.txtScan.Size = new System.Drawing.Size(462, 20);
            this.txtScan.TabIndex = 5;
            this.txtScan.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtScan_KeyDown);
            // 
            // LblDesc1
            // 
            this.LblDesc1.AutoSize = true;
            this.LblDesc1.Font = new System.Drawing.Font("VAGRounded BT", 39.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDesc1.Location = new System.Drawing.Point(189, 51);
            this.LblDesc1.Name = "LblDesc1";
            this.LblDesc1.Size = new System.Drawing.Size(266, 64);
            this.LblDesc1.TabIndex = 5;
            this.LblDesc1.Text = "Descrição";
            this.LblDesc1.Click += new System.EventHandler(this.LblDesc1_Click);
            // 
            // PbImg1
            // 
            this.PbImg1.BackColor = System.Drawing.Color.Lime;
            this.PbImg1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PbImg1.BackgroundImage")));
            this.PbImg1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PbImg1.Location = new System.Drawing.Point(0, 0);
            this.PbImg1.Name = "PbImg1";
            this.PbImg1.Size = new System.Drawing.Size(180, 180);
            this.PbImg1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PbImg1.TabIndex = 0;
            this.PbImg1.TabStop = false;
            this.PbImg1.Click += new System.EventHandler(this.PbImg1_Click);
            // 
            // LblPrice1
            // 
            this.LblPrice1.AutoSize = true;
            this.LblPrice1.Font = new System.Drawing.Font("VAGRounded BT", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPrice1.Location = new System.Drawing.Point(189, 101);
            this.LblPrice1.Name = "LblPrice1";
            this.LblPrice1.Size = new System.Drawing.Size(249, 98);
            this.LblPrice1.TabIndex = 7;
            this.LblPrice1.Text = "Preço";
            this.LblPrice1.Click += new System.EventHandler(this.LblPrice1_Click);
            // 
            // Panel
            // 
            this.Panel.BackColor = System.Drawing.Color.Transparent;
            this.Panel.Controls.Add(this.LblPrice);
            this.Panel.Controls.Add(this.LblR);
            this.Panel.Controls.Add(this.PbImg);
            this.Panel.Controls.Add(this.LblDesc);
            this.Panel.Location = new System.Drawing.Point(12, 12);
            this.Panel.Name = "Panel";
            this.Panel.Size = new System.Drawing.Size(387, 623);
            this.Panel.TabIndex = 7;
            // 
            // LblR
            // 
            this.LblR.AutoSize = true;
            this.LblR.Font = new System.Drawing.Font("VAGRounded BT", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblR.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.LblR.Location = new System.Drawing.Point(11, 538);
            this.LblR.Name = "LblR";
            this.LblR.Size = new System.Drawing.Size(68, 49);
            this.LblR.TabIndex = 4;
            this.LblR.Text = "R$";
            // 
            // Panel3
            // 
            this.Panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(246)))), ((int)(((byte)(206)))));
            this.Panel3.Controls.Add(this.LblPrice3);
            this.Panel3.Controls.Add(this.PbImg3);
            this.Panel3.Controls.Add(this.LblDesc3);
            this.Panel3.Location = new System.Drawing.Point(408, 461);
            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(573, 208);
            this.Panel3.TabIndex = 12;
            this.Panel3.Click += new System.EventHandler(this.Panel3_Click);
            // 
            // Panel2
            // 
            this.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(246)))), ((int)(((byte)(206)))));
            this.Panel2.Controls.Add(this.LblPrice2);
            this.Panel2.Controls.Add(this.PbImg2);
            this.Panel2.Controls.Add(this.LblDesc2);
            this.Panel2.Location = new System.Drawing.Point(408, 247);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(573, 208);
            this.Panel2.TabIndex = 11;
            this.Panel2.Click += new System.EventHandler(this.Panel2_Click);
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(246)))), ((int)(((byte)(206)))));
            this.Panel1.Controls.Add(this.LblPrice1);
            this.Panel1.Controls.Add(this.PbImg1);
            this.Panel1.Controls.Add(this.LblDesc1);
            this.Panel1.Location = new System.Drawing.Point(408, 33);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(573, 208);
            this.Panel1.TabIndex = 10;
            this.Panel1.Click += new System.EventHandler(this.Panel1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = global::BIPPPlay.Properties.Resources.pegpese_consulta_preco_tela_principal;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1112, 696);
            this.ControlBox = false;
            this.Controls.Add(this.Panel3);
            this.Controls.Add(this.Panel2);
            this.Controls.Add(this.Panel1);
            this.Controls.Add(this.Panel);
            this.Controls.Add(this.cmbLoja);
            this.Controls.Add(this.txtScan);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.Text = "Bipp Interface";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PbImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PbImg3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PbImg2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PbImg1)).EndInit();
            this.Panel.ResumeLayout(false);
            this.Panel.PerformLayout();
            this.Panel3.ResumeLayout(false);
            this.Panel3.PerformLayout();
            this.Panel2.ResumeLayout(false);
            this.Panel2.PerformLayout();
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PbImg;
        private System.Windows.Forms.PictureBox PbImg3;
        private System.Windows.Forms.PictureBox PbImg2;
        private System.Windows.Forms.Label LblPrice;
        private System.Windows.Forms.Label LblDesc;
        private System.Windows.Forms.Label LblPrice3;
        private System.Windows.Forms.Label LblDesc3;
        private System.Windows.Forms.Label LblPrice2;
        private System.Windows.Forms.Label LblDesc2;
        private System.Windows.Forms.TextBox txtScan;
        private System.Windows.Forms.Label LblDesc1;
        private System.Windows.Forms.PictureBox PbImg1;
        private System.Windows.Forms.Label LblPrice1;
        private System.Windows.Forms.ComboBox cmbLoja;
        private System.Windows.Forms.Panel Panel;
        private System.Windows.Forms.Label LblR;
        private System.Windows.Forms.Panel Panel3;
        private System.Windows.Forms.Panel Panel2;
        private System.Windows.Forms.Panel Panel1;
    }
}

