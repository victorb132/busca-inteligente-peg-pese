﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using BippInterface.Classes;
using System.Globalization;
using System.Threading;
using System.Speech.Synthesis;
using BIPPPlay.Properties;

namespace BippInterface
{
    public partial class Form1 : Form
    {
        DataSet DS = new DataSet();
        BIPP BIPP = new BIPP();
        Image BGI;
        Int64 id_prod;
        int id_loja;
        string[] v;
        Shop Sh = new Shop();
        Thread T;
        int vTime;
        SpeechSynthesizer reader = new SpeechSynthesizer();

        public Form1()
        {
            // Inicializa o formulário
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            //Variavel da imagem de fundo
            BGI = BackgroundImage;

            //Deixa a Text, a combo e as groups box invisível 
            txtScan.Visible = true;
            Panel.Visible = false;
            Panel1.Visible = false;
            Panel2.Visible = false;
            Panel3.Visible = false;
            cmbLoja.Visible = true;

            // Load do combo box da loja
            var DT = Sh.LoadCombo_Shop();

            while (DT.Read())
            {
                cmbLoja.Items.Add(String.Format("{0}", DT[0]));
            }

            T = new Thread(ThreadProcess);

            //Função para Redimensionar em qualquer tela
            Resizer();

        }

        private void Form1_FormClosing(object sender, EventArgs e)
        {
            T.Abort(); //Parar a Thread quando fechar o Form
        }

        private void txtScan_KeyDown(object sender, KeyEventArgs e)
        {
            //Condição da tecla enter
            if (cmbLoja.Visible == true) { return; }

            if (e.KeyCode == Keys.Enter)
            {
                //Try/Catch para tratamento de erros
                try
                {
                    //Tira o Som do sistema quando apertado a tecla enter
                    e.Handled = true;
                    e.SuppressKeyPress = true;

                    //Limpa as informações da tela
                    ClearAll();

                    //Scan do produto
                    id_prod = BIPP.LoadID(txtScan.Text);

                    //Puxa EAN e Produto ID
                    if (id_prod == 0)
                    {
                        id_prod = BIPP.LoadIDProd(Int64.Parse(txtScan.Text));

                        if (id_prod == 0)
                        {
                            MessageBox.Show("Produto não encontrado!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }

                    //Descrição e preço do produto principal
                    LblDesc.Text = BIPP.LoadDescription(id_prod);
                    LblPrice.Text = BIPP.LoadPrice(id_prod, id_loja).ToString("#0.00");
                    PbImg.Image = BIPP.LoadImage(id_prod);

                    //Cria a DT (DataTable)
                    var DT = BIPP.LoadIDEquiv(id_prod, id_loja);

                    //Matriz v para relativos
                    v = new string[3];
                    int x = 0;
                    while (DT.Read())

                    {
                        v[x] = (String.Format("{0}", DT[0]));
                        x += 1;
                    }

                    Panel.Visible = true;
                    Panel1.Visible = false;
                    Panel2.Visible = false;
                    Panel3.Visible = false;

                    //Descrição e preço do produto equivalente 1 
                    if (v[0] != null)
                    {
                        LblDesc1.Text = BIPP.LoadDescriptionEquiv(int.Parse(v[0]));
                        LblPrice1.Text = BIPP.LoadPriceEquiv(int.Parse(v[0]), id_loja).ToString("#0.00");
                        PbImg1.Image = BIPP.LoadImageEquiv(int.Parse(v[0]));
                        Panel1.Visible = true;
                    }

                    //Descrição e preço do produto equivalente 2
                    if (v[1] != null)
                    {
                        LblDesc2.Text = BIPP.LoadDescriptionEquiv(int.Parse(v[1]));
                        LblPrice2.Text = BIPP.LoadPriceEquiv(int.Parse(v[1]), id_loja).ToString("#0.00");
                        PbImg2.Image = BIPP.LoadImageEquiv(int.Parse(v[1]));
                        Panel2.Visible = true;
                    }

                    //Descrição e preço do produto equivalente 3
                    if (v[2] != null)
                    {
                        LblDesc3.Text = BIPP.LoadDescriptionEquiv(int.Parse(v[2]));
                        LblPrice3.Text = BIPP.LoadPriceEquiv(int.Parse(v[2]), id_loja).ToString("#0.00");
                        PbImg3.Image = BIPP.LoadImageEquiv(int.Parse(v[2]));
                        Panel3.Visible = true;
                    }

                    //Contador do tempo que as groups ficam visiveis
                    vTime = 15;

                    //Condição para ver se a Thread está em processo e reinicia
                    if (T.ThreadState == ThreadState.Unstarted)
                    {
                        T.Start();
                    }

                    //Anula o Background
                    BackgroundImage = Resources.pegpese_consulta_preco;
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                //Esvazia a txtScan
                txtScan.Text = "";
            }
        }

        private void ClearAll()
        {
            //Limpa todas as váriaveis
            LblDesc.Text = "";
            LblDesc1.Text = "";
            LblDesc2.Text = "";
            LblDesc3.Text = "";
            LblPrice.Text = "";
            LblPrice1.Text = "";
            LblPrice2.Text = "";
            LblPrice3.Text = "";
            PbImg.Image = null;
            PbImg1.Image = null;
            PbImg2.Image = null;
            PbImg3.Image = null;

        }

        private void PbImg1_Click(object sender, EventArgs e)
        {
            CallProd1();
        }

        private void LblDesc1_Click(object sender, EventArgs e)
        {
            CallProd1();
        }

        private void LblPrice1_Click(object sender, EventArgs e)
        {
            CallProd1();
        }

        private void Panel1_Click(object sender, EventArgs e)
        {
            CallProd1();
        }

        private void PbImg2_Click(object sender, EventArgs e)
        {
            CallProd2();
        }

        private void LblDesc2_Click(object sender, EventArgs e)
        {
            CallProd2();
        }

        private void LblPrice2_Click(object sender, EventArgs e)
        {
            CallProd2();
        }

        private void Panel2_Click(object sender, EventArgs e)
        {
            CallProd2();
        }

        private void PbImg3_Click(object sender, EventArgs e)
        {
            CallProd3();
        }

        private void LblDesc3_Click(object sender, EventArgs e)
        {
            CallProd3();
        }

        private void LblPrice3_Click(object sender, EventArgs e)
        {
            CallProd3();
        }

        private void Panel3_Click(object sender, EventArgs e)
        {
            CallProd3();
        }

        private void cmbLoja_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Condição para não clicar na combo vazia
            if (cmbLoja.Text == "") { return; }

            //Seleciona a Loja
            id_loja = int.Parse(cmbLoja.Text);

            //Deixa a combo invisível
            cmbLoja.Visible = false;
        }

        private void CallProd1()
        {
            // Scan do produto relativo
            CallProdFunc(v[0]);
        }

        private void CallProd2()
        {
            // Scan do produto relativo
            CallProdFunc(v[1]);
        }

        private void CallProd3()
        {
            // Scan do produto relativo
            CallProdFunc(v[2]);
        }

        private void CallProdFunc(string scan)
        {
            try
            {
                id_prod = Int32.Parse(scan);

                if (scan == null)
                {
                    return;
                }

                ClearAll();

                // Descrição e preço do produto principal
                LblDesc.Text = BIPP.LoadDescription(id_prod);
                LblPrice.Text = BIPP.LoadPrice(id_prod, id_loja).ToString("#0.00");
                PbImg.Image = BIPP.LoadImage(id_prod);

                // Cria a DT ( DataTable )
                var DT = BIPP.LoadIDEquiv(id_prod, id_loja);

                //Matriz v
                v = new string[3];
                int x = 0;
                while (DT.Read())

                {
                    v[x] = (String.Format("{0}", DT[0]));
                    x += 1;
                }

                vTime = 15;

                Panel1.Visible = false;
                Panel2.Visible = false;
                Panel3.Visible = false;

                string s; //Para testar se exite produto equivalente antes de ocultar GroupBox desnecessária

                // Descrição e preço do produto equivalente 1 
                s = BIPP.LoadDescriptionEquiv(int.Parse(v[0])); ;
                if (s != "")
                {
                    LblDesc1.Text = s;
                    LblPrice1.Text = BIPP.LoadPriceEquiv(int.Parse(v[0]), id_loja).ToString("#0.00");
                    PbImg1.Image = BIPP.LoadImageEquiv(int.Parse(v[0]));
                    s = "";
                    Panel1.Visible = true;
                }
                else
                {
                    return;
                }

                // Descrição e preço do produto equivalente 2
                s = BIPP.LoadDescriptionEquiv(int.Parse(v[1]));
                if (s != "")
                {
                    LblDesc2.Text = s;
                    LblPrice2.Text = BIPP.LoadPriceEquiv(int.Parse(v[1]), id_loja).ToString("#0.00");
                    PbImg2.Image = BIPP.LoadImageEquiv(int.Parse(v[1]));
                    Panel2.Visible = true;
                }
                else
                {
                    return;
                }

                // Descrição e preço do produto equivalente 3
                s = BIPP.LoadDescriptionEquiv(int.Parse(v[2]));
                if (s != "")
                {
                    LblDesc3.Text = s;
                    LblPrice3.Text = BIPP.LoadPriceEquiv(int.Parse(v[2]), id_loja).ToString("#0.00");
                    PbImg3.Image = BIPP.LoadImageEquiv(int.Parse(v[2]));
                    Panel3.Visible = true;
                }
                else
                {
                    return;
                }
            }
            catch
            {

            }
            
        }

        //Função para redimensionar o form para telas
        private void Resizer()
        {
            //Redimensiona o tamanho do Form
            WindowState = FormWindowState.Maximized;
            int w = Width;
            int h = Height;
            double vPanel = w * 0.4;

            Panel.Width = Convert.ToInt16(vPanel);
            Panel.Height = this.Height - 24;

            int EH = (Panel.Height - 36) / 4;

            //Redimensiona as groups
            Panel1.Location = new Point(Panel.Width + 24, 12 + EH + 12);
            Panel1.Height = Convert.ToInt16(EH);
            Panel1.Width = Convert.ToInt16((this.Width * 0.6) - 36);

            Panel2.Location = new Point(Panel.Width + 24, Panel1.Location.Y + EH + 12);
            Panel2.Height = Convert.ToInt16(EH);
            Panel2.Width = Convert.ToInt16((this.Width * 0.6) - 36);

            Panel3.Location = new Point(Panel.Width + 24, Panel2.Location.Y + EH + 12);
            Panel3.Height = Convert.ToInt16(EH);
            Panel3.Width = Convert.ToInt16((this.Width * 0.6) - 36);

            //Redimensiona as imgs
            PbImg.Width = Panel.Width;
            PbImg.Height = PbImg.Width;
            LblDesc.Location = new Point(12, PbImg.Height + 12);
            LblR.Location = new Point(12, PbImg.Height + LblDesc.Height + 72);
            LblPrice.Location = new Point(LblR.Location.X + LblR.Width - 14, PbImg.Height + LblDesc.Height + 24);

            PbImg1.Height = Panel1.Height;
            PbImg1.Width = PbImg1.Height;
            LblDesc1.Location = new Point(PbImg1.Width + 12, 12);
            LblPrice1.Location = new Point(PbImg1.Width + 12, LblDesc1.Height + 24);

            PbImg2.Height = Panel2.Height;
            PbImg2.Width = PbImg2.Height;
            LblDesc2.Location = new Point(PbImg2.Width + 12, 12);
            LblPrice2.Location = new Point(PbImg2.Width + 12, LblDesc2.Height + 24);

            PbImg3.Height = Panel3.Height;
            PbImg3.Width = PbImg3.Height;
            LblDesc3.Location = new Point(PbImg3.Width + 12, 12);
            LblPrice3.Location = new Point(PbImg3.Width + 12, LblDesc3.Height + 24);

        }
        
        //Thread para o tempo da group visível
        private void ThreadProcess()
        {
            while (T.ThreadState != ThreadState.Aborted)
            {
                Thread.Sleep(5000);

                if (vTime == 0)
                {
                    try
                    {

                        if (Panel.InvokeRequired)
                        {
                            Panel.BeginInvoke((MethodInvoker)delegate ()
                            {
                                Panel.Visible = false;
                                Panel1.Visible = false;
                                Panel2.Visible = false;
                                Panel3.Visible = false;
                                BackgroundImage = BGI;
                            });
                        }
                        vTime = -1;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Erro", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                }

                if (vTime > 0)
                {
                    vTime -= 5;
                }
            }
        }
    }
}